package ru.tsc.bagrintsev.tm.api.endpoint;

import jakarta.jws.WebMethod;
import jakarta.xml.ws.Service;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

import javax.xml.namespace.QName;
import java.net.URL;

public interface IAbstractEndpoint {

    @NotNull
    String SPACE = "http://endpoint.tm.bagrintsev.tsc.ru/";

    @SneakyThrows
    @WebMethod(exclude = true)
    static <T> T newInstance(
            @NotNull final String host,
            @NotNull final String port,
            @NotNull final String name,
            @NotNull final String space,
            @NotNull final String part,
            @NotNull final Class<T> clazz
    ) {
        @NotNull final String wsdl = String.format("http://%s:%s/%s?wsdl", host, port, name);
        @NotNull final URL url = new URL(wsdl);
        @NotNull final QName qName = new QName(space, part);
        return Service.create(url, qName).getPort(clazz);
    }

}
