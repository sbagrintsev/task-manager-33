package ru.tsc.bagrintsev.tm.api.endpoint;

import jakarta.jws.WebMethod;
import jakarta.jws.WebParam;
import jakarta.jws.WebService;
import jakarta.xml.ws.Service;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.dto.request.user.*;
import ru.tsc.bagrintsev.tm.dto.response.user.*;

import javax.xml.namespace.QName;
import java.net.URL;

@WebService
public interface IUserEndpoint extends IAbstractEndpoint {

    @NotNull
    String NAME = "UserEndpoint";

    @NotNull
    String PART = NAME + "Service";

    @SneakyThrows
    @WebMethod(exclude = true)
    static IUserEndpoint newInstance(
            @NotNull final String host,
            @NotNull final String port
    ) {
        return IAbstractEndpoint.newInstance(host, port, NAME, SPACE, PART, IUserEndpoint.class);
    }

    @NotNull
    @WebMethod
    UserChangePasswordResponse changePassword(
            @WebParam(name = "request", partName = "request")
            @NotNull final UserChangePasswordRequest request
    );

    @NotNull
    @WebMethod
    UserLockResponse lock(
            @WebParam(name = "request", partName = "request")
            @NotNull final UserLockRequest request
    );

    @NotNull
    @WebMethod
    UserRemoveResponse remove(
            @WebParam(name = "request", partName = "request")
            @NotNull final UserRemoveRequest request
    );

    @NotNull
    @WebMethod
    UserSetRoleResponse setRole(
            @WebParam(name = "request", partName = "request")
            @NotNull final UserSetRoleRequest request
    );

    @NotNull
    @WebMethod
    UserSignUpResponse signUp(
            @WebParam(name = "request", partName = "request")
            @NotNull final UserSignUpRequest request
    );

    @NotNull
    @WebMethod
    UserUnlockResponse unlock(
            @WebParam(name = "request", partName = "request")
            @NotNull final UserUnlockRequest request
    );

    @NotNull
    @WebMethod
    UserUpdateProfileResponse updateProfile(
            @WebParam(name = "request", partName = "request")
            @NotNull final UserUpdateProfileRequest request
    );

}
