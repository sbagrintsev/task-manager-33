package ru.tsc.bagrintsev.tm.api.endpoint;

import jakarta.jws.WebMethod;
import jakarta.jws.WebParam;
import jakarta.jws.WebService;
import jakarta.xml.ws.Service;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.dto.request.data.*;
import ru.tsc.bagrintsev.tm.dto.response.data.*;

import javax.xml.namespace.QName;
import java.net.URL;

@WebService
public interface IDomainEndpoint extends IAbstractEndpoint {

    @NotNull
    String NAME = "DomainEndpoint";

    @NotNull
    String PART = NAME + "Service";

    @SneakyThrows
    @WebMethod(exclude = true)
    static IDomainEndpoint newInstance(
            @NotNull final String host,
            @NotNull final String port
    ) {
        return IAbstractEndpoint.newInstance(host, port, NAME, SPACE, PART, IDomainEndpoint.class);
    }

    @NotNull
    @WebMethod
    BackupLoadResponse loadBackup(
            @WebParam(name = "request", partName = "request")
            @NotNull final BackupLoadRequest request
    );

    @NotNull
    @WebMethod
    BackupSaveResponse saveBackup(
            @WebParam(name = "request", partName = "request")
            @NotNull final BackupSaveRequest request
    );

    @NotNull
    @WebMethod
    DataBase64LoadResponse loadBase64(
            @WebParam(name = "request", partName = "request")
            @NotNull final DataBase64LoadRequest request
    );

    @NotNull
    @WebMethod
    DataBase64SaveResponse saveBase64(
            @WebParam(name = "request", partName = "request")
            @NotNull final DataBase64SaveRequest request
    );

    @NotNull
    @WebMethod
    DataBinaryLoadResponse loadBinary(
            @WebParam(name = "request", partName = "request")
            @NotNull final DataBinaryLoadRequest request
    );

    @NotNull
    @WebMethod
    DataBinarySaveResponse saveBinary(
            @WebParam(name = "request", partName = "request")
            @NotNull final DataBinarySaveRequest request
    );

    @NotNull
    @WebMethod
    DataJacksonJsonLoadResponse loadJacksonJson(
            @WebParam(name = "request", partName = "request")
            @NotNull final DataJacksonJsonLoadRequest request
    );

    @NotNull
    @WebMethod
    DataJacksonJsonSaveResponse saveJacksonJson(
            @WebParam(name = "request", partName = "request")
            @NotNull final DataJacksonJsonSaveRequest request
    );

    @NotNull
    @WebMethod
    DataJacksonXmlLoadResponse loadJacksonXml(
            @WebParam(name = "request", partName = "request")
            @NotNull final DataJacksonXmlLoadRequest request
    );

    @NotNull
    @WebMethod
    DataJacksonXmlSaveResponse saveJacksonXml(
            @WebParam(name = "request", partName = "request")
            @NotNull final DataJacksonXmlSaveRequest request
    );

    @NotNull
    @WebMethod
    DataJacksonYamlLoadResponse loadJacksonYaml(
            @WebParam(name = "request", partName = "request")
            @NotNull final DataJacksonYamlLoadRequest request
    );

    @NotNull
    @WebMethod
    DataJacksonYamlSaveResponse saveJacksonYaml(
            @WebParam(name = "request", partName = "request")
            @NotNull final DataJacksonYamlSaveRequest request
    );

    @NotNull
    @WebMethod
    DataJaxbJsonLoadResponse loadJaxbJson(
            @WebParam(name = "request", partName = "request")
            @NotNull final DataJaxbJsonLoadRequest request
    );

    @NotNull
    @WebMethod
    DataJaxbJsonSaveResponse saveJaxbJson(
            @WebParam(name = "request", partName = "request")
            @NotNull final DataJaxbJsonSaveRequest request
    );

    @NotNull
    @WebMethod
    DataJaxbXmlLoadResponse loadJaxbXml(
            @WebParam(name = "request", partName = "request")
            @NotNull final DataJaxbXmlLoadRequest request
    );

    @NotNull
    @WebMethod
    DataJaxbXmlSaveResponse saveJaxbXml(
            @WebParam(name = "request", partName = "request")
            @NotNull final DataJaxbXmlSaveRequest request
    );

}
