package ru.tsc.bagrintsev.tm.api.endpoint;

import jakarta.jws.WebMethod;
import jakarta.jws.WebParam;
import jakarta.jws.WebService;
import jakarta.xml.ws.Service;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.dto.request.system.AboutRequest;
import ru.tsc.bagrintsev.tm.dto.request.system.InfoRequest;
import ru.tsc.bagrintsev.tm.dto.request.system.VersionRequest;
import ru.tsc.bagrintsev.tm.dto.response.system.ServerAboutResponse;
import ru.tsc.bagrintsev.tm.dto.response.system.ServerInfoResponse;
import ru.tsc.bagrintsev.tm.dto.response.system.ServerVersionResponse;

import javax.xml.namespace.QName;
import java.net.URL;

@WebService
public interface ISystemEndpoint extends IAbstractEndpoint {

    @NotNull
    String NAME = "SystemEndpoint";

    @NotNull
    String PART = NAME + "Service";

    @SneakyThrows
    @WebMethod(exclude = true)
    static ISystemEndpoint newInstance(
            @NotNull final String host,
            @NotNull final String port
    ) {
        return IAbstractEndpoint.newInstance(host, port, NAME, SPACE, PART, ISystemEndpoint.class);
    }

    @NotNull
    @WebMethod
    ServerAboutResponse getAbout(
            @WebParam(name = "request", partName = "request")
            @NotNull final AboutRequest request
    );

    @NotNull
    @WebMethod
    ServerVersionResponse getVersion(
            @WebParam(name = "request", partName = "request")
            @NotNull final VersionRequest request
    );

    @NotNull
    @WebMethod
    ServerInfoResponse getInfo(
            @WebParam(name = "request", partName = "request")
            @NotNull final InfoRequest request
    );

}
