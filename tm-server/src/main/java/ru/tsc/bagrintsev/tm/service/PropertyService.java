package ru.tsc.bagrintsev.tm.service;

import com.jcabi.manifests.Manifests;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.api.sevice.IPropertyService;

import java.util.Properties;

public class PropertyService implements IPropertyService {

    @NotNull
    public static final String FILE_NAME = "application.properties";

    @NotNull
    public static final String APPLICATION_VERSION_KEY = "buildNumber";

    @NotNull
    public static final String AUTHOR_EMAIL_KEY = "email";

    @NotNull
    public static final String AUTHOR_NAME_KEY = "developer";

    @NotNull
    public static final String PASSWORD_HASH_ITERATIONS_DEFAULT = "65536";

    @NotNull
    public static final String PASSWORD_HASH_ITERATIONS_KEY = "passwordHash.iterations";

    @NotNull
    public static final String PASSWORD_HASH_KEY_LENGTH_DEFAULT = "128";

    @NotNull
    public static final String PASSWORD_HASH_KEY_LENGTH_KEY = "passwordHash.keyLength";

    @NotNull
    public static final String SERVER_HOST_KEY = "server.host";

    @NotNull
    public static final String SERVER_HOST_DEFAULT = "localhost";

    @NotNull
    public static final String SERVER_PORT_KEY = "server.port";

    @NotNull
    public static final String SERVER_PORT_DEFAULT = "6060";

    @NotNull
    public static final String SESSION_KEY = "session.key";

    @NotNull
    public static final String SESSION_TIMEOUT_KEY = "session.timeout";

    @NotNull
    public static final String EMPTY_VALUE = "---";

    @NotNull
    public final Properties properties = new Properties();

    @SneakyThrows
    public PropertyService() {
        properties.load(ClassLoader.getSystemResourceAsStream(FILE_NAME));
    }

    @NotNull
    private String getEnvKey(@NotNull final String key) {
        return key.replace(".", "_").toUpperCase();
    }

    @NotNull
    private String getStringValue(
            @NotNull final String key,
            @NotNull final String defaultValue
    ) {
        @NotNull final String envKey = getEnvKey(key);
        if (System.getProperties().containsKey(key)) return System.getProperties().getProperty(key);
        if (System.getenv().containsKey(envKey)) return System.getenv(envKey);
        return properties.getProperty(key, defaultValue);
    }

    @NotNull
    @Override
    public String getApplicationVersion() {
        try {
            @NotNull final String version = Manifests.read(APPLICATION_VERSION_KEY);
            return version;
        } catch (IllegalArgumentException e) {
            return "1.31.test";
        }
    }

    @NotNull
    @Override
    public String getAuthorName() {
        try {
            @NotNull final String name = Manifests.read(AUTHOR_NAME_KEY);
            return name;
        } catch (IllegalArgumentException e) {
            return "testName";
        }
    }

    @NotNull
    @Override
    public String getAuthorEmail() {
        try {
            @NotNull final String email = Manifests.read(AUTHOR_EMAIL_KEY);
            return email;
        } catch (IllegalArgumentException e) {
            return "testEmail";
        }
    }

    @NotNull
    @Override
    public Integer getPasswordHashIterations() {
        @NotNull final String value = getStringValue(PASSWORD_HASH_ITERATIONS_KEY, PASSWORD_HASH_ITERATIONS_DEFAULT);
        return Integer.parseInt(value);
    }

    @NotNull
    @Override
    public Integer getPasswordHashKeyLength() {
        @NotNull final String value = getStringValue(PASSWORD_HASH_KEY_LENGTH_KEY, PASSWORD_HASH_KEY_LENGTH_DEFAULT);
        return Integer.parseInt(value);
    }

    @NotNull
    @Override
    public String getServerHost() {
        @NotNull final String value = getStringValue(SERVER_HOST_KEY, SERVER_HOST_DEFAULT);
        return value;
    }

    @NotNull
    @Override
    public Integer getServerPort() {
        @NotNull final String value = getStringValue(SERVER_PORT_KEY, SERVER_PORT_DEFAULT);
        return Integer.parseInt(value);
    }

    @NotNull
    @Override
    public String getSessionKey() {
        @NotNull final String value = getStringValue(SESSION_KEY, EMPTY_VALUE);
        return value;
    }

    @NotNull
    @Override
    public Integer getSessionTimeout() {
        @NotNull final String value = getStringValue(SESSION_TIMEOUT_KEY, EMPTY_VALUE);
        return Integer.parseInt(value);
    }

}
