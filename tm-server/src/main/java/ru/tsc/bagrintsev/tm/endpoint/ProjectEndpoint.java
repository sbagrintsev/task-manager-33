package ru.tsc.bagrintsev.tm.endpoint;

import jakarta.jws.WebMethod;
import jakarta.jws.WebParam;
import jakarta.jws.WebService;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.api.endpoint.IProjectEndpoint;
import ru.tsc.bagrintsev.tm.api.sevice.IProjectService;
import ru.tsc.bagrintsev.tm.api.sevice.IServiceLocator;
import ru.tsc.bagrintsev.tm.dto.request.project.*;
import ru.tsc.bagrintsev.tm.dto.response.project.*;
import ru.tsc.bagrintsev.tm.enumerated.Role;
import ru.tsc.bagrintsev.tm.enumerated.Sort;
import ru.tsc.bagrintsev.tm.enumerated.Status;
import ru.tsc.bagrintsev.tm.model.Project;

import java.util.List;

@NoArgsConstructor
@WebService(endpointInterface = "ru.tsc.bagrintsev.tm.api.endpoint.IProjectEndpoint")
public final class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    public ProjectEndpoint(@NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    private IProjectService getProjectService() {
        return getServiceLocator().getProjectService();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @NotNull ProjectChangeStatusByIdResponse changeProjectStatusById(
            @WebParam(name = PARAM, partName = PARAM)
            @NotNull final ProjectChangeStatusByIdRequest request
    ) {
        @Nullable final String userId = check(request, Role.values()).getUserId();
        @Nullable final String id = request.getId();
        @Nullable final String statusValue = request.getStatusValue();
        @Nullable final Status status = Status.toStatus(statusValue);
        @NotNull final Project project = getProjectService().changeProjectStatusById(userId, id, status);
        return new ProjectChangeStatusByIdResponse(project);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @NotNull ProjectChangeStatusByIndexResponse changeProjectStatusByIndex(
            @WebParam(name = PARAM, partName = PARAM)
            @NotNull final ProjectChangeStatusByIndexRequest request
    ) {
        @Nullable final String userId = check(request, Role.values()).getUserId();
        @Nullable final Integer index = request.getIndex();
        @Nullable final String statusValue = request.getStatusValue();
        @Nullable final Status status = Status.toStatus(statusValue);
        @NotNull final Project project = getProjectService().changeProjectStatusByIndex(userId, index, status);
        return new ProjectChangeStatusByIndexResponse(project);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @NotNull ProjectClearResponse clearProject(
            @WebParam(name = PARAM, partName = PARAM)
            @NotNull final ProjectClearRequest request
    ) {
        @Nullable final String userId = check(request, Role.values()).getUserId();
        getProjectService().clear(userId);
        return new ProjectClearResponse();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @NotNull ProjectCreateResponse createProject(
            @WebParam(name = PARAM, partName = PARAM)
            @NotNull final ProjectCreateRequest request
    ) {
        @Nullable final String userId = check(request, Role.values()).getUserId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @NotNull final Project project = getProjectService().create(userId, name, description);
        return new ProjectCreateResponse(project);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @NotNull ProjectListResponse listProject(
            @WebParam(name = PARAM, partName = PARAM)
            @NotNull final ProjectListRequest request
    ) {
        @Nullable final String userId = check(request, Role.values()).getUserId();
        @Nullable final String sortValue = request.getSortValue();
        @Nullable final Sort sort = Sort.toSort(sortValue);
        @Nullable final List<Project> projects = getProjectService().findAll(userId, sort);
        return new ProjectListResponse(projects);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @NotNull ProjectRemoveByIdResponse removeProjectById(
            @WebParam(name = PARAM, partName = PARAM)
            @NotNull final ProjectRemoveByIdRequest request
    ) {
        @Nullable final String userId = check(request, Role.values()).getUserId();
        @Nullable final String id = request.getId();
        @NotNull final Project project = getProjectService().findOneById(userId, id);
        getServiceLocator().getProjectTaskService().removeProjectById(userId, project.getId());
        return new ProjectRemoveByIdResponse(project);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @NotNull ProjectRemoveByIndexResponse removeProjectByIndex(
            @WebParam(name = PARAM, partName = PARAM)
            @NotNull final ProjectRemoveByIndexRequest request
    ) {
        @Nullable final String userId = check(request, Role.values()).getUserId();
        @Nullable final Integer index = request.getIndex();
        @NotNull final Project project = getProjectService().findOneByIndex(userId, index);
        serviceLocator.getProjectTaskService().removeProjectById(userId, project.getId());
        return new ProjectRemoveByIndexResponse(project);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @NotNull ProjectShowByIdResponse showProjectById(
            @WebParam(name = PARAM, partName = PARAM)
            @NotNull final ProjectShowByIdRequest request
    ) {
        @Nullable final String userId = check(request, Role.values()).getUserId();
        @Nullable final String id = request.getId();
        @NotNull final Project project = getProjectService().findOneById(userId, id);
        return new ProjectShowByIdResponse(project);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @NotNull ProjectShowByIndexResponse showProjectByIndex(
            @WebParam(name = PARAM, partName = PARAM)
            @NotNull final ProjectShowByIndexRequest request
    ) {
        @Nullable final String userId = check(request, Role.values()).getUserId();
        @Nullable final Integer index = request.getIndex();
        @NotNull final Project project = getProjectService().findOneByIndex(userId, index);
        return new ProjectShowByIndexResponse(project);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @NotNull ProjectUpdateByIdResponse updateProjectById(
            @WebParam(name = PARAM, partName = PARAM)
            @NotNull final ProjectUpdateByIdRequest request
    ) {
        @Nullable final String userId = check(request, Role.values()).getUserId();
        @Nullable final String id = request.getId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @NotNull final Project project = getProjectService().updateById(userId, id, name, description);
        return new ProjectUpdateByIdResponse(project);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @NotNull ProjectUpdateByIndexResponse updateProjectByIndex(
            @WebParam(name = PARAM, partName = PARAM)
            @NotNull final ProjectUpdateByIndexRequest request
    ) {
        @Nullable final String userId = check(request, Role.values()).getUserId();
        @Nullable final Integer index = request.getIndex();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @NotNull final Project project = getProjectService().updateByIndex(userId, index, name, description);
        return new ProjectUpdateByIndexResponse(project);
    }
}
