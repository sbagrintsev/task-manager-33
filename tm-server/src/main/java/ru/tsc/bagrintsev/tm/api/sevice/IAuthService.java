package ru.tsc.bagrintsev.tm.api.sevice;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.dto.request.AbstractUserRequest;
import ru.tsc.bagrintsev.tm.enumerated.Role;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.model.Session;
import ru.tsc.bagrintsev.tm.model.User;

import java.security.GeneralSecurityException;

public interface IAuthService extends Checkable {

    @NotNull
    String signIn (
            @Nullable String login,
            @Nullable String password
    );

    @NotNull
    Session validateToken(@Nullable String token);

    @NotNull
    String getUserId(@NotNull AbstractUserRequest request);

    @NotNull
    User checkUser(
            @Nullable final String login,
            @Nullable final String password
    ) throws AbstractException, GeneralSecurityException;

}
