package ru.tsc.bagrintsev.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.dto.request.system.InfoRequest;
import ru.tsc.bagrintsev.tm.dto.response.system.ServerInfoResponse;
import ru.tsc.bagrintsev.tm.exception.AbstractException;

public class InfoCommand extends AbstractSystemCommand {

    @Override
    public void execute() throws AbstractException {
        showOperationInfo();
        @Nullable final ServerInfoResponse response = getSystemEndpoint().getInfo(new InfoRequest());
        @NotNull final String processors = String.format("Available processors (cores): %s", response.getProcessors());
        @NotNull final String freeMemory = String.format("Free memory: %s", response.getFreeMemory());
        @NotNull final String maxMemory = String.format("Maximum memory: %s", response.getMaxMemory());
        @NotNull final String totalMemory = String.format("Total memory available to JVM: %s", response.getTotalMemory());
        @NotNull final String usedMemory = String.format("Used memory in JVM: %s", response.getUsedMemory());
        System.out.printf("%s\n%s\n%s\n%s\n%s\n", processors, maxMemory, totalMemory, freeMemory, usedMemory);
    }

    @NotNull
    @Override
    public String getName() {
        return "info";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "-i";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Print system info.";
    }

}
