package ru.tsc.bagrintsev.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.dto.request.data.DataJacksonXmlSaveRequest;

public final class DataJacksonXmlSaveCommand extends AbstractDataCommand {

    @Override
    @SneakyThrows
    public void execute() {
        showOperationInfo();
        getDomainEndpoint().saveJacksonXml(new DataJacksonXmlSaveRequest(getToken()));
    }

    @NotNull
    @Override
    public String getName() {
        return "data-save-xml";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Save current application state in xml file";
    }

}
