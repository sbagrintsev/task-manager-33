package ru.tsc.bagrintsev.tm.command;

import jakarta.xml.bind.JAXBException;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.api.model.ICommand;
import ru.tsc.bagrintsev.tm.api.sevice.IServiceLocator;
import ru.tsc.bagrintsev.tm.enumerated.EntityField;
import ru.tsc.bagrintsev.tm.enumerated.Role;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.exception.system.ServiceLocatorNotInitializedException;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Locale;

public abstract class AbstractCommand implements ICommand {

    @Nullable
    protected IServiceLocator serviceLocator;

    public abstract void execute() throws IOException, AbstractException, GeneralSecurityException, ClassNotFoundException, JAXBException;

    @NotNull
    public abstract String getName();

    @NotNull
    public abstract String getShortName();

    @NotNull
    public abstract String getDescription();

    @NotNull
    public abstract Role[] getRoles();

    public IServiceLocator getServiceLocator() throws AbstractException {
        if (serviceLocator == null) throw new ServiceLocatorNotInitializedException();
        return serviceLocator;
    }

    public void setServiceLocator(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @SneakyThrows
    @Nullable
    protected String getToken() {
        return getServiceLocator().getTokenService().getToken();
    }

    @SneakyThrows
    protected void setToken(@Nullable final String token) {
        getServiceLocator().getTokenService().setToken(token);
    }

    protected void showOperationInfo() {
        System.out.printf("[%s]%n", getName().toUpperCase(Locale.ROOT).replace('-', ' '));
    }

    protected void showParameterInfo(@NotNull final EntityField parameter) {
        System.out.printf("Enter %s: %n", parameter.getDisplayName());
    }

    @NotNull
    @Override
    public String toString() {
        @NotNull StringBuilder result = new StringBuilder();
        result.append("[");
        result.append(String.format("%-35s", getName()));
        result.append(" | ");
        result.append(String.format("%-25s", getShortName()));
        result.append("]");
        if (!getDescription().isEmpty()) {
            while (result.length() < 75) {
                result.append(" ");
            }
            result.append(getDescription());
        }
        return result.toString();
    }

}
